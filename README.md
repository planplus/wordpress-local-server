# Wordpress local server

This project is a simple javascript node server project which uses Express which is a minimal and flexible Node.js web application framework

## Install

`npm install`

## Start Local server

Run `node index` for a loca server. Navigate to `http://localhost:8086/`. For any change of code we recommend to start the server

## Code scaffolding

Modify index.js to use the captured payload from wordpress. Save the file. Start server.

## SP configuration
Open application-local.yml
Replace following two lines
`wordpressLink: https://dev.planplusglobal.com/wp-json/ppg/v1/customerstatus/`
`wordpressTokenAPI: https://dev.planplusglobal.com/wp-json/api/v1/token`ß
  By
`wordpressLink: http://localhost:8086/getjson/`
`wordpressTokenAPI: http://localhost:8086/authenticatetoken`

## get the jwt token from https://jwt.io/ web site
An image jwt.png is a sample jwt token
# payload
{
  "sub": {"userid":["991"]},
  "name": "John Doe",
  "iat": 1516239022
}
# VERIFY SIGNATURE
## Use secret key 
C4B003D1BF16FE747DA747F447A8524AFEE1D56761936420BDAFCB10ECCD784E
## secret base64 checkbox should be checked
Copy the jwt token and assign to attrToken parameter
https://localhost/api/rest/v30/sso/saml?attrToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsidXNlcmlkIjpbIjk5MSJdfSwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.gB_DhJZ6rIQm6FHATCMNPW50qwbfdILUy8AIwrhskq0

# Run the API application
# Open browser check following http://localhost:8086/getjson/991 works
# Finally copy https://localhost/api/rest/v30/sso/saml?attrToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsidXNlcmlkIjpbIjk5MSJdfSwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.gB_DhJZ6rIQm6FHATCMNPW50qwbfdILUy8AIwrhskq0 and press enter. Set a debug break point in SSOController.java /saml end point. Start your debug from here. If API returns success, you will be redirected to 
https://localhost/suitpro/advisor/dashboard?sso=true and app will land to user dashboard.

# Done
