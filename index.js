var express = require('express');
var app = express();

//Will be used in SP-API application-local.yml file wordpressTokenAPI: http://localhost:8086/authenticatetoken
app.post("/authenticatetoken", function (req, res) {
    let payload  = {"id_token":""};
    res.status(200).json(payload);
});

//Will be used in SP-API application-local.yml file wordpressLink: http://localhost:8086/getjson/
app.get("/getjson/:userId", function (req, res) {

    let payload = 

    {
        "userid": "991",
        "ppg_userid": "NA",
        "first_name": "zzz",
        "last_name": "zzz",
        "email": "zzz.zzz@zzz.com",
        "phone": "0000000000",
        "company": "ZZZ Partners",
        "street_address": "",
        "unit_number": "",
        "city": "",
        "region": "IL",
        "country": "US",
        "postal_zip": "",
        "modules": [
            {
                "module": "profiler",
                "user_status": "trialist",
                "trial_start_date": "2019-11-01 00:00:00+00:00",
                "trial_end_date": "2020-08-31 00:00:00+00:00",
                "subscription_start_date": "",
                "subscription_end_date": ""
            },
            {
                "module": "protracker",
                "user_status": "trialist",
                "trial_start_date": "2019-11-01 00:00:00+00:00",
                "trial_end_date": "2020-08-31 00:00:00+00:00",
                "subscription_start_date": "",
                "subscription_end_date": ""
            }
        ],
        "trial_teamid": "3192",
        "trial_team_name": "ZZZ Partners",
        "trial_team_owner_id": "991",
        "teamID": null,
        "team_name": null,
        "team_role": null,
        "team_owner_id": null,
        "affiliation": "",
        "membership_status": null,
        "subscription_status": null,
        "subscription_cancelled_date": null,
        "order_id": null,
        "subscription_qty": null,
        "subscription_total": null,
        "customer_language": "NA",
        "WP_role": "trialist"
    };
   res.setHeader('Content-Type', 'application/json')
   res.status(200).send(payload);
});


app.listen(8086, function () {
    console.log('app listening on port 8086!');
   });

module.exports = app;